# Main ideas

We split the data in 11 categories of prices in Paris
We exclude rooms wich have a price over 400€/night



# Project structure

## data 
    - brut : data collect the web
    - train : normalized data for training
    - test : normalized data for test

## models : 
    - Different models computed

## src
    - data.py : normalized data
    - train.py : train model and generate de h5 file with the algorithm used in class
    - test.py : test the model with the test structure
    -train_test.py : TRUE file to train and test the model with different method