import seaborn as sns
import pandas as pd
import numpy as np 
from math import isnan
chemin_date = "./data/brut/"
data = pd.read_csv(chemin_date+'airbnb_brut.csv')


neighbourhood = data["neighbourhood"]
latitude = data["latitude"]
longitude=data["longitude"]
accommodates=data["accommodates"]
bathrooms=data["bathrooms"]
bedrooms=data["bedrooms"]
beds=data["beds"]
price=data["price"]
review_scores_rating=data["review_scores_rating"]
room_type = data["room_type"]


# Etude pour les locations entières ou les pièces uniquemement
rate=[0 for k in range(12)]
chambre = []
for room in room_type:
    if type(room) == str:
        if room == "Entire home/apt":
            chambre.append(1)
        else:
            if room == "Private room":
                chambre.append(2)
            else:
                chambre.append(pd.NaT)
    else:
        chambre.append(pd.NaT)

print(len(chambre) == len(room_type))

#Cast prix 
pri = []
for p in price:
    if "$" in p:
        if "," in p:
            p = p.replace(',','')
        pri.append(float(p.replace('$', '')))
    else:
        pri.append(pd.NaT)
prix = []
maxi=max(pri)
print(np.mean(pri))
print(np.median(pri))

for pr in pri:
    if pr>400:
        prix.append(pd.NaT)
        rate[11]+=1
    else:
        toAdd = int(10*float(pr)/(400))
        prix.append(toAdd)
    rate[toAdd]+=1
for i in range(len(rate)):
    print (str(i)+' '+str(rate[i]))

print(len(prix)==len(price))
quartier = []
for city in neighbourhood:

    if type(city)==str and "Arrondissement" in city:
        el = city.split(" ")[0]
        if el == 'I':
            quartier.append(1) 
        if el == 'II':
            quartier.append(2)
        if el == 'III':
            quartier.append(3)
        if el == 'IV':
            quartier.append(4)
        if el == 'V':
            quartier.append(5)
        if el == 'VI':
            quartier.append(6)
        if el == 'VII':
            quartier.append(7)
        if el == 'VIII':
            quartier.append(8)
        if el == 'IX':
            quartier.append(9)
        if el == 'X':
            quartier.append(10)
        if el == 'XI':
            quartier.append(11)
        if el == 'XII':
            quartier.append(12)
        if el == 'XIII':
            quartier.append(13)
        if el == 'XIV':
            quartier.append(14)
        if el == 'XV':
            quartier.append(15)
        if el == 'XVI':
            quartier.append(16)
        if el == 'XVII':
            quartier.append(17)
        if el == 'XVIII':
            quartier.append(18)
        if el == 'XIX':
            quartier.append(19)
        if el == 'XX':
            quartier.append(1)
    else:
        quartier.append(pd.NaT)
print(len(quartier)==len(neighbourhood))


nbr_train = int(0.8*len(data))
training = pd.DataFrame({'quartier': quartier[0:nbr_train],
                   'latitude': latitude[0:nbr_train],
                   'longitude': longitude[0:nbr_train],
                   'accomodates': accommodates[0:nbr_train],
                   'bathrooms': bathrooms[0:nbr_train],
                   'bedrooms': bedrooms[0:nbr_train],
                   'beds': beds[0:nbr_train],
                   'review': review_scores_rating[0:nbr_train],
                   'chambre': chambre[0:nbr_train],
                   'price': prix[0:nbr_train]
                   })
training.dropna(inplace=True)
training.to_csv("./data/train/air_train.csv",index=False)

test = pd.DataFrame({'quartier': quartier[nbr_train+1:-1],
                   'latitude': latitude[nbr_train+1:-1],
                   'longitude': longitude[nbr_train+1:-1],
                   'accomodates': accommodates[nbr_train+1:-1],
                   'bathrooms': bathrooms[nbr_train+1:-1],
                   'bedrooms': bedrooms[nbr_train+1:-1],
                   'beds': beds[nbr_train+1:-1],
                   'review': review_scores_rating[nbr_train+1:-1],
                   'chambre': chambre[nbr_train+1:-1],
                   'price': prix[nbr_train+1:-1]
                   })
test.dropna(inplace=True)
test.to_csv("./data/test/air_test.csv",index=False)


