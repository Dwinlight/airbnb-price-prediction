import numpy as np
import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
# load dataset
dataframe = pandas.read_csv("./data/train/air_train.csv")
dataset = dataframe.values

datatest = pandas.read_csv("./data/train/air_train.csv")
datasettest = datatest.values


X = dataset[:,0:9].astype(float)
x_test = datasettest[:,0:9].astype(float)
print(X)
Y = dataset[:,-1]
Y_test = datasettest[:,-1]
print(Y)

i = 0
check = [False, False,False,False,False,False,False,False,False,False,False,False]
while i<len(Y) and check != [True,True,True,True,True,True,True,True,True,True,True,True]:
	if not(check[int(Y[i])]):
		check[int(Y[i])] = True
	i+=1
n_dimension = 0
for el in check:
	if el:
		n_dimension+=1
print(n_dimension)
# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)
model = None
# define baseline model

	# create model
model = Sequential()
model.add(Dense(11, input_dim=9, activation='relu'))

model.add(Dense(11, input_dim=9, activation='relu'))
model.add(Dense(11, input_dim=9, activation='relu'))
model.add(Dense(n_dimension, activation='softmax'))
# Compile model
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])	
model.fit(X, dummy_y, epochs=100)
model.save_weights("models/model_fit.h5")
#model.load_weights("model_air.h5")


rez = model.predict_classes(x_test)

ratio = 0


for i in range(len(Y_test)):
    if (int(rez[i]) <= int(Y_test[i])+1) and  (int(rez[i]) >= int(Y_test[i])-1):
        ratio+=1
print(100*ratio/len(Y_test))

ratio = 0


for i in range(len(Y_test)):
    if (int(rez[i]) ==int(Y_test[i])):
        ratio+=1
print(100*ratio/len(Y_test))



#print(model.predict_classes(np.array([14 48.492 2.2018 2 1 1 1 4.75 1])))