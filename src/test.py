import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline

data = pandas.read_csv("./data/train/air_train.csv")
dataset = data.values
intputs = dataset[0:1000,0:7].astype(float)
Y = dataset[0:1000,-1]
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)
model = Sequential()
model.add(Dense(11, input_dim=7, activation='relu'))
model.add(Dense(11, activation='softmax'))
model.load_weights("model_air.h5")

rez = model.predict_classes(intputs)
ratio = 0
print("dummy")
print(dummy_y)
print("rerz")
print(rez)
for i in range(len(dummy_y)):
    if int(rez[i]) == int(dummy_y[i]):
        ratio+=1
print(100*ratio/len(out))