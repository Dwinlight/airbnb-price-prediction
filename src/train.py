import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
# load dataset
dataframe = pandas.read_csv("./data/train/air_train.csv")
dataset = dataframe.values
X = dataset[0:1000,0:7].astype(float)

print(X)
Y = dataset[0:1000,-1]
print(Y)

i = 0
check = [False, False,False,False,False,False,False,False,False,False,False,False]
while i<len(Y) and check != [True,True,True,True,True,True,True,True,True,True,True,True]:
	if not(check[int(Y[i])]):
		check[int(Y[i])] = True
	i+=1
n_dimension = 0
for el in check:
	if el:
		n_dimension+=1
print(n_dimension)
# encode class values as integers
encoder = LabelEncoder()
encoder.fit(Y)
encoded_Y = encoder.transform(Y)
# convert integers to dummy variables (i.e. one hot encoded)
dummy_y = np_utils.to_categorical(encoded_Y)
model = None
# define baseline model
def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(8, input_dim=7, activation='relu'))
	model.add(Dense(11, activation='softmax'))
	# Compile model
	model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
	return model
 
estimator = KerasClassifier(build_fn=baseline_model, epochs=200, batch_size=5, verbose=1)
kfold = KFold(n_splits=10, shuffle=False)
results = cross_val_score(estimator, X, dummy_y, cv=kfold)
print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))
print("n_dim = "+str(n_dimension))


